= Safe boot sequence =
by Zygo Blaxell <mailtoo@furryterror.org>

A central problem in using a Unix SBC as the forebrain for a UPS is
that the power could drop out from under it at any time,  If this happens
during mass-storage I/O, there is a risk that the filesystem will be
left in a damaged state, causing failure to boot cleanly on the next 
powerup.

To avoid this, the forebrain power-up sequence is designed so the
forebrain filesystems exist entirely in RAM with no dependency on the
boot SD card during normal operation.  The SD card includes both the
OS and the UPS control software in kernel and initramfs files.

At boot, u-boot loads the kernel and initramfs into memory, then the
kernel unpacks the initramfs file (a cpio.xz archive) into a ramfs
mounted on /.  ramfs uses host RAM to store files, so there is no
wasted space and no access latency.  xz and cpio provide data
integrity checks so that we will not run a corrupted kernel or
userspace filesystem.

For a KISS method of firmware update, users can remove the SD card,
pop it into any machine with Internet access and a SD card slot,
download new firmware files onto the SD card, pop it back into the
UPSide forebrain, and restart.  Only u-boot needs to read the files,
so in simple upgrade cases users don't need specialized tools to make
an SD card image.  The filesystem on the SD card can be vfat, so
merely copying the files with standard OS utilities will do.

For live firmware updates, a new firmware file can be downloaded to
the forebrain and written to the SD card.  After clearing caches and
re-reading the files to validate their correctness, the firmware can
then reboot itself to do a kernel upgrade.  To upgrade
userspace, unpack the initramfs, replace the files in the system's
ramfs, and restart daemons.

For live system recovery, if the initramfs contains fdisk, mkfs, a
u-boot installer utility, and a copy of the kernel, then the system
can reformat a corrupted or replacement SD card and write an entirely
new boot filesystem on it using the existing system running in RAM.

Requirement: In all cases where the forebrain is writing to its boot
SD card, the forebrain shall verify sufficient battery charge to
operate the forebrain until the writes are completed.  If sufficient
charge is not available, upgrades will be deferred.

For logging, a separate tmpfs should be used rather than writing to
the initramfs.  tmpfs has a mount option to limit the amount of RAM it
uses to store files; ramfs does not.  RAM is limited (there can be no
swap) and files occupy RAM, so anything that writes files needs to be
strictly scrutinized.

For debugging and development, the kernel can include a USB CDC gadget
driver (which makes the UPS monitoring host see a USB-Ethernet device
inside the UPS through its USB cable) or a USB CDC host driver (which
allows the UPS monitoring host to use an attached USB-Ethernet dongle
and connect to wired Ethernet).  Either one provides a network
interface over which tools like ssh, rsync, git, rsyslog, etc. can be
used to move software into the forebrain and data out.

We use initramfs the same way Debian initramfs-tools does: load up a
minimal set of kernel modules, scan for usable filesystems and hotplug
devices, and mount root... except we don't bother mounting a root
filesystem, we just keep running straight out of initramfs.

A single-initramfs requires that all of userspace must fit inside the
RAM of the device; however, the devices we are considering for the
forebrain have 512MB of RAM or more, so this should not be an onerous
constraint (even if we have multiple copies of userspace in RAM,
e.g. during upgrades and development).  All realtime control software
must fit entirely within RAM anyway; otherwise, its responses would be
delayed by demand paging the code from a filesystem.

After boot time, the UPS management software runs in userspace as a
daemon.  Other UPS subsystems are controlled through
https://www.kernel.org/doc/Documentation/i2c/dev-interface[the i2c-dev
interface].

The consequence of this design is that power outages to the forebrain
crash it without leaving persistent scrambled data that could
compromise operation after the next powerup.  For extra safety, reads
and writes to the EEPROM should use and verify a checksum on each
piece of retained data, falling back to safe defaults.

There are "high-endurance" SD cards intended for dashcams.  Using one
of these might be a good idea...

For modularity, the midbrain does not initially know anything else's
I2C address. It gets that information from the forebrain during
boot. That way the midbrain firmware doesn't have to be modified when
we change components. And the forebrain gets it from a configuration
file that the main service daemon reads at boot time, describing the
hardware configuration.

// end
