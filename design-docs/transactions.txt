= Transactional design of UPSide =

This document describes the behavior and state transitions of the
UPSide.  It is deliberately not an ECAD design and includes no part
numbers; the intent is to specify system partitioning and behavior,
not implementation.

== Forebrain/midbrain/hindbrain partitioning ==

For reasons including but not limited to by the cost of getting
regulatory approvals, the electronics is partitioned into low-power
and high-power electronics with very narrow interfaces among them.

Any change to the high-power subsystems could require a new approval
cycle. Therefore, as much as possible of the system logic - and all
policy logic - lives in the low-power part.  The high-power subsystem
is as dumb and simple as possible; keeping it dumb and simple is
a principal design goal.

The "midbrain" is the line-voltage unit: AC in, AC out, DC in, DC
out, and status/control lines to the forebrain. As little policy as
possible is done here, since any change probably leads to a
recertification cycle. It is where the "safety" rules are enforced,
like only one source feeding the equipment. The midbrain needs to fail
to a safe state if both battery and mains current die.

The "forebrain" is responsible for external interfacing, including
driving the status display and the USB port. Battery state modeling is
done here as well; the battery voltage/discharge/temperature sensors
report to it. (In a design with a smart battery, case 2, modeling
would be done in the inboard controller; in a design for case 3 the
BMS would be separate and probably physically located with the
high-power electronics.)

The "hindbrain" is the battery management system.

The UPSide design uses an embedded Unix computer as a forebrain, which
puts some constraints on it. It can't have a filesystem that is both
writeable and persistent across boots - otherwise the UPS will
eventually fall in a cryptic Unix way after a power outage.  We solve
this problem with a boot sequence based on initramfs.

Everything else is driven by the forebrain, either directly or through
controls exported from the midbrain.

== Battery management ==

An important variable of any UPS design is where the battery
management is done.  There are are three cases:

1. LiFePo batteries normally ship with a battery controller on a
little PCB inboard of the battery. Its purpose in life is to enforce a
cutoff voltage so the battery doesn't get deep-cycled, and rate-limit
charging of the battery so it doesn't get damaged.  The interface to
the rest of the system is just 4 DC connections; the inboard
controller doesnt ship any data out.

2. Some batteries, especially lithium-ion batteries, ship with a much
smarter inboard battery controller that has a digital channel supplying
detailed statistics and battery modeling.  The interaces to these
batteries are highly variable and some use networking such as CANbus -
probably each individual kind would require a custom UPS designed
around it.

3. Lead-acid batteries, including the SLAs and gel-cel variants used
in UPSes, usually have no inboard BMS at all - the DC input and output
go straight to the plates. The UPS has to supply its own BMS, but at
least in this case it's possible to standardize one.

We refer to the battery controller as the "hindbrain".  The UPSide-1
design assumes case 1, a dumb controller, with DC in and out,
inboard of the battery.  Thus, we don't have to design it, just be
sure the pack has one and match the input and output spec voltage to
the rest of the system.

Batteries for case 3 would be cheaper, but we want to avoid the
liability and complexity issues of building our own BMS. Besides,
LiFePo buys more service lifetime per dollar.

== Subsytems ==

Forebrain::
An SBC running Unix.  After boot it becomes the master controller.

Midbrain::
Controller for the high-power subsystem.  Handles boot.

Display::
Human-readable status display.  A 20x4 LCD panel or something in that class.

Buzzer::
Alarm-tone generator, controlled by midbrain.

Bus::
Data bus connecting the subsystems - I2C, SPI, or something equivalent.

The following diagram shows data flow among the low-power components.
Power connections are not indicated.  Solid arrows carry
message traffic, dotted arrows are DC control.

image:paths.svg[]

== State diagram ==

In the following state diagram, ovals are are states and boxes are
state transition events.

"Bus:" lines in events are identifiers for event notifications.
Semantics of event notifications is described in a later section.

"Display:" lines in events are text to be sent to the status display.

"Alarm:" lines in events are alarm codes.  Those are described in a
later section.

Not shown is that in normal operation the midbrain periodically sends
POWER messages to the forebrain.

Not shown is that in normal operation the forebrain periodically sends
ALIVE to the midbrain to prevent the watchdog from rebooting the daemon.

image:flow.png[]

The reason for the distinction between primary and secondary charge
delay is this: If AC is reliable, after powering the UPS up odds are high
AC will remain good long enough for the battery to charge at least enough to
sustain the host through a shutdown cycle, thus the primary charge delay
cam usually be zero.  On the other hand, if the UPS just had to issue
shutdown, (a) the battery is mostly drained, and (b) we know AC has
recently been unreliable.  Thus we want a charge delay with a nonzero
default that gives the battery some recovery time.

== Bus messages and alarm codes ==

[options="header"]
|========================================================
|Code     | From      | To        | Payload    
|CONFIG   | Forebrain | Midbrain  | Configuration info; elicits VERSION
|BUZZ     | Forebrain | Midbrain  | Buzzer tone sequence
|ALIVE    | Forebrain | Midbrain  | Prevent watchdog reboot
|DELAY1   | Forebrain | Midbrain  | Set primary charge-delay interval (default 0s)
|DELAY2   | Forebrain | Midbrain  | Set secondary charge-delay interval (default 60s)
|POWER    | Midbrain  | Forebrain | Power source state (see below).
|CHARGING | Midbrain  | Forebrain | Charge delay entered
|MAINSUP  | Midbrain  | Forebrain | Mains power becomes available
|MAINSDOWN| Midbrain  | Forebrain | Mains power becomes unavailable
|SHUTDOWN | Midbrain  | Forebrain | Request shutdown send to host
|VERSION  | Midbrain  | Forebrain | Ship firmware revision ID
|DISPLAY  | Forebrain | Display   | Text for display
|========================================================

=== CONFIG ===

The configuration info has these fields:

* I2C address of forebrain
* Battery nominal output voltage
* Battery cutoff voltage
* Charging voltage - low end
* Charging voltage - high end
* Ampere-hour rating 

Before CONFIG the midbrain assumes a nominal cutoff voltage of 12v
for the purpose of deciding whether the battery can deliver current.

=== POWER ===

The POWER message describes the state of MAINS and BATTERY sources.
It is sent both periodically and after MAINSUP/MAINSDOWN. Fields are as follows:

* Mains voltage
* Battery voltage
* Battery current - signed, - for discharge, + for charge
* Temperature

Note: this field inventory assumes that the battery is *not* accessed
through a smart BMS (battery management system) and the forebrain has to do
state-of-charge modeling itself. This assumption is appropriate for "dumb"
LiFePo batteries, which for safety reasons have a dumb BMS inboard
enforcing a charge-current limit and cutoff voltage.

It's not enough for SLAs, nor for naked lithium cells (we don't plan
to support the latter for aforementioned safety reasons).  SLAs and
naked lithium will require a BMS in the midbrain itself.

Some lithium batteries include a smart BMS on board. Data shipped by a
smart BMS typically includes fields such as the following:

* Temperature
* Pack voltage
* Current (signed + charge, - discharge)
* SoC % (State of charge, percentage of full capacity available.)
* Cell voltage [6]
* Cycle count
* Nominal capacity (mAh)
* Actual capacity (mAh)
* Serial number
* Lifecycle percentage

The above is the inventory from a DJI TB47S LiPo battery.  Talking to
a smart BMS is not yet supported but we document the difference in
assumptions towards the day it is.

=== MAINSUP and MAINSDOWN ===

Notification messages shipped when mains ability changes.  Both are
immediately followed by a POWER message.

=== BUZZ ===

BUZZ is what the forebrain uses to ring alarms; all alarms except FAIL
are done this way.  It is used to implement the following:

|========================================================
|FAIL	  | Boot failure                | F in Morse code
|STARTING | Mains power on              | HI in Morse code
|UP       | Mains power restored        | U in Morse code
|DOWN     | Going to battery power      | D in Morse code
|CHARGING | Waiting on battery charge   | C in Morse code
|SOS      | Dwell limit approaching     | SOS in Morse code
|========================================================



// end
